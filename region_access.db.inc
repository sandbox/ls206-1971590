<?php

/**
 * @file
 * Database function for the region_access module.
 */

/**
 * Save a region group definition
 */
function region_access_group_save($data) {
  // check for gid and update
  if (isset($data['gid'])) {
    db_update('region_access_groups')
      ->fields(array(
        'group_name' => $data['group_name'],
        'country_codes' => serialize($data['country_codes']),
      ))
      ->condition('gid', $data['gid'], '=')
      ->execute();
  }
  // otherwise insert a new group
  else {
    db_insert('region_access_groups')
      ->fields(array(
        'group_name' => $data['group_name'],
        'country_codes' => serialize($data['country_codes']),
      ))
      ->execute();
  }
}

/**
 * Retrieve a region group definition
 */
function region_access_load_group($gid) {
  $group = db_select('region_access_groups', 'g')
    ->fields('g')
    ->condition('gid', $gid, '=')
    ->execute();
  return $group->fetchAssoc();
}

/**
 * Retrieve all defined region groups
 */
function region_access_get_all_groups() {
  $groups = db_select('region_access_groups', 'g')
    ->fields('g')
    ->execute();
  return $groups;
}

/**
 * Delete a region group definition
 */
function region_access_delete_group($gid) {
  db_delete('region_access_groups')
    ->condition('gid', $gid, '=')
    ->execute();
}

/**
 * Add or update a record in the region_access table
 * @param $nid
 * @param $gid
 */
function region_access_save_node_access($nid, $gid) {
  if ($gid) {
    // Find out if there is already a region_access record.
    $exists =  db_query('SELECT nid FROM {region_access} WHERE nid = :nid', array(':nid' => $nid))->fetchField();

    // If there is already a record, update it with the new private value.
    if ($exists) {
      $num_updated = db_update('region_access')
        ->fields(array(
          'nid' => $nid,
          'gid' => $gid,
        ))
        ->condition('nid', $nid)
        ->execute();
    }
    // Otherwise, create a new record.
    else {
      db_insert('region_access')
        ->fields(array(
          'nid' => $nid,
          'gid' => $gid,
        ))
        ->execute();
    }
  }
}

/**
 * Delete a record in the region_access table
 * @param $nid
 */
function region_access_delete_node_access($nid) {
  db_delete('region_access')
    ->condition('nid', $nid, '=')
    ->execute();
}

/**
 * Save a record into the countries table
 * @param $data
 */
function region_access_save_country($data) {
  db_insert('region_access_countries')
    ->fields(array(
      'country_code' => $data['country_code'],
      'country_name' => $data['country_name'],
    ))
    ->execute();
}

/**
 * Load the countries and codes
 *
 * @return array
 */
function region_access_load_contries_array() {
  $records = db_select('region_access_countries', 'c')
    ->fields('c', array(
      'country_code',
      'country_name',
    ))
    ->execute();
  $countries = array();
  while ($row = $records->fetchAssoc()) {
    $countries[$row['country_code']] = $row['country_name'];
  }
  return $countries;
}

/**
 * Load a cid from the countries table
 * @param $country_code
 * @return mixed
 */
function region_access_load_country_id($country_code) {
  $record = db_select('region_access_countries', 'c')
    ->fields('c')
    ->condition('country_code', $country_code, '=')
    ->execute();
  $cid = $record->fetchField();
  return $cid;
}

/**
 * Return the block id from the module and delta values
 *
 * @param $module
 * @param $delta
 * @return mixed
 */
function region_access_get_block_id($module, $delta) {
  $bid = db_select('block', 'b')
    ->fields('b', array('bid'))
    ->condition('module', $module, '=')
    ->condition('delta', $delta, '=')
    ->execute()->fetchAssoc();
  return $bid;
}

/**
 * Save a block region access record
 *
 * @param $bid
 * @param $gid
 */
function region_access_save_block_access($bid, $gid) {
  if ($gid !== 0) {
    // Find out if there is already a region_access record.
    $exists =  db_query('SELECT bid FROM {region_access_block} WHERE bid = :bid', array(':bid' => $bid))->fetchField();

    // If there is already a record, update it with the new private value.
    if ($exists) {
      $num_updated = db_update('region_access_block')
        ->fields(array(
          'bid' => $bid,
          'gid' => $gid,
        ))
        ->condition('bid', $bid)
        ->execute();
    }
    // Otherwise, create a new record.
    else {
      db_insert('region_access_block')
        ->fields(array(
          'bid' => $bid,
          'gid' => $gid,
        ))
        ->execute();
    }
    // save into table
  }
}

/**
 * Return the group id for the given block id
 *
 * @param $bid
 * @return mixed
 */
function region_access_load_block_gid($bid) {
  $gid = db_select('region_access_block', 'b')
    ->fields('b')
    ->condition('bid', $bid, '=')
    ->execute()->fetchAssoc();
  return $gid['gid'];
}

/**
 * Delete a region access block record
 *
 * @param $bid
 */
function region_access_block_delete($bid) {
  db_delete('region_access_block')
    ->condition('bid', $bid, '=')
    ->execute();
}

/**
 * Retrieve the block id from a custom block with a given delta
 *
 * @param $delta
 * @return mixed
 */
function region_access_get_custom_block($delta) {
  $bid = db_select('block', 'b')
    ->fields('b')
    ->condition('module', 'block', '=')
    ->condition('delta', $delta, '=')
    ->execute()->fetchAssoc();
  return $bid['bid'];
}