<?php

/**
 * @file
 * Admin forms for the region_access module.
 */

/**
 * Form to define region groups
 */
function region_access_admin_form($form, &$form_state) {
  $form = array();

  $form['region_groups'] = array(
    '#type' => 'vertical_tabs',
  );

  $groups = region_access_get_all_groups();
  while ($row = $groups->fetchAssoc()) {
    $form['group_set_' . $row['gid']] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($row['group_name']),
      '#group' => 'region_groups',
      '#collapsible' => TRUE,
      '#tree' => TRUE,
    );
    $form_state['storage']['group_set_' . $row['gid']] = $row['gid'];
    $form['group_set_' . $row['gid']]['group_id'] = array(
      '#type' => 'hidden',
      '#value' => $row['gid'],
    );
    $form['group_set_' . $row['gid']]['group_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Group name'),
      '#default_value' => $row['group_name'],
      '#maxlength' => 255,
      '#required' => TRUE,
    );
    $countries = unserialize($row['country_codes']);
    $form['group_set_' . $row['gid']]['countries'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Countries'),
      '#options' => region_access_load_contries_array(),
      '#default_value' => $countries,
      '#required' => TRUE,
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update groups',
  );

  return $form;
}

/**
 * Form validate function for region_access_admin_form.
 */
function region_access_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values as $name => $vals) {
    if (preg_match('/group_set_[0-9]*/', $name)) {
      $country_codes = array_filter($vals['countries']);
      if (empty($country_codes)) {
        form_set_error($name . '][countries', 'You must select at least one option');
      }
      if (count($vals['group_name']) > 255) {
        form_set_error($name . '][group_name', 'You must select at least one option');
      }
    }
  }
}

/**
 * Form submit function for region_access_admin_form.
 */
function region_access_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  foreach ($values as $name => $vals) {
    if (preg_match('/group_set_[0-9]*/', $name)) {
      $country_codes = array_filter($vals['countries']);
      $data = array(
        'gid' => $form_state['storage'][$name],
        'group_name' => $vals['group_name'],
        'country_codes' => $country_codes,
      );
      region_access_group_save($data);
    }
  }
}

/**
 * Form callback for menu item admin/config/people/region_access/add
 */
function region_access_add_new_form() {
  $form = array();

  $form['group_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Group name'),
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['countries'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Countries'),
    '#options' => region_access_load_contries_array(),
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add new group'),
  );

  return $form;
}

/**
 * Form submit function for region_access_add_new_form.
 */
function region_access_add_new_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $group_name = $values['group_name'];
  $countries = array_filter($values['countries']);
  $data = array(
    'group_name' => $group_name,
    'country_codes' => $countries,
  );
  // save the group
  region_access_group_save($data);
  drupal_set_message(t('The new group has been saved'));
  $form_state['redirect'] = 'admin/config/people/region_access';
}