<?php

/**
 * @file
 * Block specific functions for the region_access module.
 */

/**
 * Implements hook_block_view_alter().
 */
function region_access_block_view_alter(&$data, $block) {
  if (isset($_SESSION['smart_ip']['location']['country_code'])) {
    $user_region = $_SESSION['smart_ip']['location']['country_code'];
    $bid = region_access_get_block_id($block->module, $block->delta);
    if ($gid = region_access_load_block_gid($bid)) {
      $group = region_access_load_group($gid);
      $codes = unserialize($group['country_codes']);
      // if access is restricted
      if (!in_array($user_region, $codes)) {
        $data['subject'] = '';
        $data['content'] = '';
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function region_access_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'block_custom_block_delete') {
    // add to beginning of array because we need to query the block table
    array_unshift($form['#submit'], 'region_access_block_delete_submit');
  }
  if ($form_id == 'block_add_block_form' || $form_id == 'block_admin_configure') {
    // Fieldset to contain the region access options
    // Use the group additional settings to display as a vertical tab
    $form['region_access'] = array(
      '#type' => 'fieldset',
      '#title' => t('Location access'),
      '#collapsible' => TRUE,
      '#weight' => 8,
      '#group' => 'visibility',
      '#access' => user_access('assign region access'),
    );
    // if updating a block
    if ($form['delta']['#value']) {
      $bid = region_access_get_block_id($form['module']['#value'], $form['delta']['#value']);
      $gid = region_access_load_block_gid($bid);
    }
    // Allow users with permissions to set the access level
    $form['region_access']['region_access_group'] = array(
      '#type' => 'select',
      '#title' => t('Group access'),
      '#description' => t('Select a region group to restrict this content to. Set to none to remove any access controls.'),
      '#default_value' => isset($gid) ? $gid : FALSE,
      '#options' => region_access_get_group_options(),
      '#access' => user_access('assign region access'),
    );
    $form['#submit'][] = 'region_access_block_form_submit';
  }
}

/**
 * Submit callback for the block add and configure forms.
 */
function region_access_block_form_submit($form, &$form_state) {
  $access_group = $form_state['values']['region_access_group'];
  if ($access_group) {
    $module = $form_state['values']['module'];
    $delta = $form_state['values']['delta'];
    // get the block id from the module and delta values
    $bid = region_access_get_block_id($module, $delta);
    // save the region access group with the block id
    region_access_save_block_access($bid, $access_group);
  }
}

/**
 * Submit callback
 */
function region_access_block_delete_submit($form, &$form_state) {
  // get the block id from the module and delta values
  $bid = region_access_get_custom_block($form_state['values']['bid']);
  region_access_block_delete($bid);
}